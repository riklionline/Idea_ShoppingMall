package com.remcal.info;

import java.util.Objects;

/**
 * @ClassName: AuthInfo
 * @Descirption:
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 12/15/2019 5:38 PM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
public class AuthInfo {

    private int auid;
    private String auname;
    private String aupath;
    private int aupid;
    private short autype;

    public int getAuid() {
        return auid;
    }

    public void setAuid(int auid) {
        this.auid = auid;
    }

    public String getAuname() {
        return auname;
    }

    public void setAuname(String auname) {
        this.auname = auname;
    }

    public String getAupath() {
        return aupath;
    }

    public void setAupath(String aupath) {
        this.aupath = aupath;
    }

    public int getAupid() {
        return aupid;
    }

    public void setAupid(int aupid) {
        this.aupid = aupid;
    }

    public short getAutype() {
        return autype;
    }

    public void setAutype(short autype) {
        this.autype = autype;
    }

    @Override
    public String toString() {
        return "AuthInfo{" +
                "auid=" + auid +
                ", auname='" + auname + '\'' +
                ", aupath='" + aupath + '\'' +
                ", aupid=" + aupid +
                ", autype=" + autype +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AuthInfo authInfo = (AuthInfo) o;
        return auid == authInfo.auid &&
                aupid == authInfo.aupid &&
                autype == authInfo.autype &&
                Objects.equals(auname, authInfo.auname) &&
                aupath.equals(authInfo.aupath);
    }

    @Override
    public int hashCode() {
        return Objects.hash(auid, auname, aupath, aupid, autype);
    }
}
