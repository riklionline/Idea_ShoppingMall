package com.remcal.info;


import com.remcal.code.MVCViewSrting;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @ClassName: AdminInfo
 * @Descirption: 前台数据（与数据库实体类分离）
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 12/13/2019 3:35 PM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
public class AdminInfo {

    private int aid;//主键
    private String aname;//管理员名字
    private String aacount;//用户名
    private String apass;//密码
    private String astatus;//状态启用
    private String aphone;
    private String aemail;
    private Date ahiredate;//入职时间

    private boolean islogin = false;
    private int pageNum = MVCViewSrting.NUMBER_ONE;
    private int pageSize = MVCViewSrting.NUMBER_FIVE;

    private Date astart;
    private Date aend;

    private List<RoleInfo> roles;

    public List<RoleInfo> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleInfo> roles) {
        this.roles = roles;
    }

    public Date getAstart() {
        return astart;
    }

    public void setAstart(Date astart) {
        this.astart = astart;
    }

    public Date getAend() {
        return aend;
    }

    public void setAend(Date aend) {
        this.aend = aend;
    }

    public Date getAhiredate() {
        return ahiredate;
    }

    public void setAhiredate(Date ahiredate) {
        this.ahiredate = ahiredate;
    }

    public String getAstatus() {
        return astatus;
    }

    public void setAstatus(String astatus) {
        this.astatus = astatus;
    }


    public String getAemail() {
        return aemail;
    }

    public void setAemail(String aemail) {
        this.aemail = aemail;
    }

    public String getAphone() {
        return aphone;
    }

    public void setAphone(String aphone) {
        this.aphone = aphone;
    }

    public boolean isIslogin() {
        return islogin;
    }

    public void setIslogin(boolean islogin) {
        this.islogin = islogin;
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getAacount() {
        return aacount;
    }

    public void setAacount(String aacount) {
        this.aacount = aacount;
    }

    public String getApass() {
        return apass;
    }

    public void setApass(String apass) {
        this.apass = apass;
    }

    @Override
    public String toString() {
        return "AdminInfo{" +
                "aid=" + aid +
                ", aname='" + aname + '\'' +
                ", aacount='" + aacount + '\'' +
                ", apass='" + apass + '\'' +
                ", astatus='" + astatus + '\'' +
                ", aphone='" + aphone + '\'' +
                ", aemail='" + aemail + '\'' +
                ", ahiredate=" + ahiredate +
                ", islogin=" + islogin +
                ", pageNum=" + pageNum +
                ", pageSize=" + pageSize +
                ", astart=" + astart +
                ", aend=" + aend +
                ", roles=" + roles +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AdminInfo adminInfo = (AdminInfo) o;
        return aid == adminInfo.aid &&
                islogin == adminInfo.islogin &&
                pageNum == adminInfo.pageNum &&
                pageSize == adminInfo.pageSize &&
                Objects.equals(aname, adminInfo.aname) &&
                Objects.equals(aacount, adminInfo.aacount) &&
                Objects.equals(apass, adminInfo.apass) &&
                Objects.equals(astatus, adminInfo.astatus) &&
                Objects.equals(aphone, adminInfo.aphone) &&
                Objects.equals(aemail, adminInfo.aemail) &&
                Objects.equals(ahiredate, adminInfo.ahiredate) &&
                Objects.equals(astart, adminInfo.astart) &&
                Objects.equals(aend, adminInfo.aend) &&
                roles.equals(adminInfo.roles);
    }

    @Override
    public int hashCode() {
        return Objects.hash(aid, aname, aacount, apass, astatus, aphone, aemail, ahiredate, islogin, pageNum, pageSize, astart, aend, roles);
    }
}
