package com.remcal.entity;

/**
 * @ClassName: AdminEntity
 * @Descirption:
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 12/14/2019 11:32 AM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
public class AdminEntity {

    private int aid;//主键
    private String aname;//管理员名字
    private String aacount;//用户名
    private String apass;//密码


    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    public String getAacount() {
        return aacount;
    }

    public void setAacount(String aacount) {
        this.aacount = aacount;
    }

    public String getApass() {
        return apass;
    }

    public void setApass(String apass) {
        this.apass = apass;
    }

    @Override
    public String toString() {
        return "AdminInfo{" +
                "aid=" + aid +
                ", aname='" + aname + '\'' +
                ", aacount='" + aacount + '\'' +
                ", apass='" + apass + '\'' +
                '}';
    }

}
