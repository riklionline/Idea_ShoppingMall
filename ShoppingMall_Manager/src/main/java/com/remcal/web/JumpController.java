package com.remcal.web;

import com.github.pagehelper.PageInfo;
import com.remcal.code.MVCViewSrting;
import com.remcal.info.AdminInfo;
import com.remcal.info.RoleInfo;
import com.remcal.service.IAdminService;
import com.remcal.util.CustomerDateConvert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: JumpController
 * @Descirption:
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 12/14/2019 3:35 PM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */

@Controller
public class JumpController {

    @Autowired
    IAdminService adminService;

    /**
     * 跳转至登陆界面
     * @return
     */
    @RequestMapping("jumpLogin")
    public String jumpLogin(){

        System.out.println("Step1:开始访问登陆页面------------------------------");
        return MVCViewSrting.VIEW_LOGIN;
    }

    /**
     * 跳转至欢迎页面
     * @return
     */
    @RequestMapping("welcome")
    public String welcome(){
        System.out.println("Step4:开始访问欢迎页面------------------------------");
        return MVCViewSrting.VIEW_WELCOME;
    }

    /**
     * 跳转至管理员列表
     * @return
     */
    @RequestMapping("jumpAdmin")
    public String jumpAdmin(AdminInfo adminInfo, Model model){
        System.out.println("前台输入的admin信息数据："+adminInfo);

        List<AdminInfo> adminlist1 = adminService.queryAdmins(adminInfo);
        List<AdminInfo> adminlist2 = adminService.queryAdminsRoles(adminInfo);

        for (AdminInfo admins1 : adminlist1) {
            System.out.println("管理员列表信息[无角色]："+admins1);
            int aid = admins1.getAid();
            for (AdminInfo admins2 : adminlist2) {
                System.out.println("管理员角色信息[仅有角色]："+admins2);
                if (aid==admins2.getAid()) {
                    List<RoleInfo> roles = admins2.getRoles();
                    admins1.setRoles(roles);
                    System.out.println("补充角色数据后的管理员信息："+admins1);
                    break;
                }
            }
        }

        PageInfo<AdminInfo> pageInfo = new PageInfo<>(adminlist1);
        model.addAttribute(MVCViewSrting.VIEW_ADMINPAGEINFO,pageInfo);

        //页面数据回填，每次查询时，都要把上一次产生的最新“输入数据”用于本次输入的数据，不然sql查询条件会使用初始状态的数据
        model.addAttribute("adminInfo", adminInfo);

        return MVCViewSrting.VIEW_ADMIN;//跳转至admin.jsp

    }

    /**
     *
     * @return
     */
    @RequestMapping("jumpRole")
    public String jumpRole(){
        return MVCViewSrting.VIEW_ROLE;
    }

    /**
     *
     * @return
     */
    @RequestMapping("jumpAuth")
    public String jumpAuth(){
        return MVCViewSrting.VIEW_AUTH;
    }

    /**
     *
     * @return
     */
    @RequestMapping("jumpAuthType")
    public String jumpAuthType(){
        return MVCViewSrting.VIEW_AUTHTYPE;
    }

}
