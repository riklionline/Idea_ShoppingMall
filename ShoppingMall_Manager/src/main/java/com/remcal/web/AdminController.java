package com.remcal.web;

import com.remcal.info.AdminInfo;
import com.remcal.info.AuthInfo;
import com.remcal.info.RoleInfo;
import com.remcal.service.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;
import com.remcal.code.MVCViewSrting;
import com.remcal.util.StringUtil;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @ClassName: AdminController
 * @Descirption:
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 12/13/2019 1:24 PM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
@Controller
@SessionAttributes({"admin","auths"})
public class AdminController {

    @Autowired
    IAdminService adminService;

    /**
     * 管理员登陆方法
     * @param adminInfo
     * @return
     */
    @RequestMapping("adminLogin")
    public ModelAndView adminLogin(AdminInfo adminInfo){
        System.out.println("Step2:管理员正在尝试登陆------------------------------");
        System.out.println("Step2:客户端向后台发送数据："+adminInfo);

        ModelAndView mv = new ModelAndView(MVCViewSrting.VIEW_LOGIN);//项目规定字符串标准化

        if (null == adminInfo) {
            mv.addObject(MVCViewSrting.MSG_ERROR, MVCViewSrting.MSG_ERROR_NULL);//项目规定字符串标准化,后期需要使用资源文件中程序国际化
            return mv;
        }

        if (StringUtil.isEmpty(adminInfo.getAacount())) {
            mv.addObject(MVCViewSrting.MSG_ERROR, MVCViewSrting.MSG_ERROR_NAME_NULL);//项目规定字符串标准化,后期需要使用资源文件中程序国际化
            return mv;
        }

        if (StringUtil.isEmpty(adminInfo.getApass())) {
            mv.addObject(MVCViewSrting.MSG_ERROR, MVCViewSrting.MSG_ERROR_PASS_NULL);//项目规定字符串标准化,后期需要使用资源文件中程序国际化
            return mv;
        }

        /**
         * 调用service方法进行登陆验证
         */
        AdminInfo admin = adminService.queryAdminInfoByNameAndPass(adminInfo);

        if (null == admin) {

            //这里的提示与上面有所不同，这里是执行了查询方法后，发现数据库里没有这个账号信息
            mv.addObject(MVCViewSrting.MSG_ERROR, MVCViewSrting.MSG_ERROR_WRONG);
            return mv;
        }

        /**
         * 登陆成功，返回index首页
         */
        System.out.println("Step3:查询账号信息成功："+admin);
        System.out.println("Step3:"+admin.getAname()+"开始访问首页---------");

        //获取用户所有权限集合，包括“权限”和“菜单”
        Map<String,Set> map = getAuthsByAdmin(admin);
        mv.addObject("auths", map.get("auths"));//把登陆的admin的所有authPath放入session
        mv.addObject("menus", map.get("menus"));//把登陆的admin的所有authType放入session

        mv.addObject(MVCViewSrting.VIEW_ADMIN, admin);//admin作为一个完整的数据模型对象封装传递给页面
        mv.setViewName(MVCViewSrting.VIEW_INDEX);
        return mv;
    }

    private Map<String,Set> getAuthsByAdmin(AdminInfo admin) {

        Map<String,Set> map = new HashMap<>();
        Set<String> set = new HashSet<>();
        Set<AuthInfo> menus = new HashSet<>();

        for (RoleInfo role : admin.getRoles()) {
            for (AuthInfo auth : role.getAuths()) {
                set.add(auth.getAupath());
                if (auth.getAutype() == 1) {
                    menus.add(auth);
                }
            }
        }

        map.put("auths", set);
        map.put("menus",menus);

        return map;
    }
}
