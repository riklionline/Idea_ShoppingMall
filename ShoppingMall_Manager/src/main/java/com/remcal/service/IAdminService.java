package com.remcal.service;

import com.remcal.info.AdminInfo;
import com.remcal.info.RoleInfo;

import java.util.List;

/**
 * @InterfaceName: IAdminService
 * @Descirption:
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 12/13/2019 1:25 PM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
public interface IAdminService {

    public AdminInfo queryAdminInfoByNameAndPass(AdminInfo adminInfo);
    public List<AdminInfo> queryAdmins(AdminInfo adminInfo);
    public List<AdminInfo> queryAdminsRoles(AdminInfo adminInfo);

}
