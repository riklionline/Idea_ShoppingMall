package com.remcal.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.remcal.info.AdminInfo;
import com.remcal.info.RoleInfo;
import com.remcal.mapper.AdminMapper;
import com.remcal.service.IAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName: AdminServiceImpl
 * @Descirption:
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 12/13/2019 1:26 PM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
@Service
public class AdminServiceImpl implements IAdminService {

    @Autowired
    AdminMapper adminMapper;

    public AdminInfo queryAdminInfoByNameAndPass(AdminInfo adminInfo) {
        return adminMapper.loginQueryAuth(adminInfo);
    }

    @Override
    public List<AdminInfo> queryAdmins(AdminInfo adminInfo) {
        PageHelper.startPage(adminInfo.getPageNum(), adminInfo.getPageSize());//分页查询条件设置
        return adminMapper.queryAdmins(adminInfo);
    }

    @Override
    public List<AdminInfo> queryAdminsRoles(AdminInfo adminInfo) {
        //PageHelper.startPage(adminInfo.getPageNum(), adminInfo.getPageSize());//分页查询条件设置
        return adminMapper.queryAdminsRoles(adminInfo);
    }
}
