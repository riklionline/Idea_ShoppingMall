package com.remcal.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @ClassName: AuthFilter
 * @Descirption:
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 12/15/2019 6:50 PM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
public class AuthFilter implements Filter {

    //1、不拦截的静态资源，定义一个字符串
    static final String static_url=".html.jsp.css.js.ttf.woff.jpg.jpeg.font.png.bmp";
    //2、不拦截的动态请求，放入字符串list中
    List<String> list = new ArrayList<>();

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        list.add("/login");
        list.add("/welcome");
        list.add("/adminLogin");
        list.add("/jumpLogin");
        list.add("/regist");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = ((HttpServletRequest) servletRequest);
        HttpServletResponse response = ((HttpServletResponse) servletResponse);

        String servletPath = request.getServletPath();
        System.out.println("过滤器拦截ServletPath："+servletPath);

        //判断当前的请求是否是对静态资源进行访问（尾缀路径中包含"."的即静态资源，如.html.jsp等，相对静态资源的请求是对控制器请求）
        if (servletPath.lastIndexOf(".") > -1){
            //进一步判断这个静态资源的请求是不是我们之前定义的
            if (static_url.indexOf(servletPath.substring(servletPath.lastIndexOf(".")))>-1){
                //放行
                System.out.println("过滤器静态资源放行ServletPath："+servletPath);
                filterChain.doFilter(request, response);
                return;
            }
        }

        boolean flag = false;
        for (String str : list) {
            if (str.equals(servletPath)) {
                flag = true;
                break;
            }
        }
        if (flag) {
            //放行
            System.out.println("过滤器动态资源放行ServletPath："+servletPath);
            filterChain.doFilter(request, response);
            return;
        }

        //上面代码是过滤器不进行拦截的，直接放行，下面是剩下的被拦截的请求，校验用户是否有权限执行这个功能
        Set<String> auths = (Set<String>) request.getSession().getAttribute("auths");
        if (null == auths) {
            System.out.println("当前管理员'auth'为空………………………………………………");
            response.sendRedirect("jumpLogin");
            return;
        }

        flag = false;
        for (String str : auths) {
            if (str.equals(servletPath)) {
                flag = true;
            }
        }

        if (flag) {
            filterChain.doFilter(request, response);
        }else {
            response.sendRedirect("noauth.jsp");
        }
    }

    @Override
    public void destroy() {

    }
}
