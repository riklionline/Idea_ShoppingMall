<%--
  Created by IntelliJ IDEA.
  Author: FAT-Remcal
  Date: 12/15/2019
  Time: 8:43 PM
  Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>NOAUTH</title>
    <style>
        div {
            width: 300px;
            height: 300px;
            margin: 0 auto;
            position: relative;
            top: 30%;
        }
    </style>
</head>
<body>
<div class="noauth">
    <h1>无访问权限！</h1>
</div>

</body>
</html>
