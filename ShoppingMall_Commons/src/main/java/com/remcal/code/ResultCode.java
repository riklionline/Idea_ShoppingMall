package com.remcal.code;

/**
 * @ClassName: ResultCode
 * @Descirption:
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 12/13/2019 12:41 PM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
public class ResultCode {

    public static final String SHOP_RESULT_FALL = "100001";//系统异常
    public static final String SHOP_RESULT_SUCESS = "100000";//请求成功

    public static final int SHOP_RESULT_NUMBER_ONE = 1;
    public static final int SHOP_RESULT_NUMBER_TWO = 2;
    public static final int SHOP_RESULT_NUMBER_THREE = 3;

}
