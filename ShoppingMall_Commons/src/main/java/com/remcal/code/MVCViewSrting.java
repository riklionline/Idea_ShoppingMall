package com.remcal.code;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @ClassName: MVCViewSrting
 * @Descirption:
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 12/13/2019 3:46 PM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
public class MVCViewSrting {

    public static final String MSG_ERROR = "errorMsg";
    public static final String MSG_ERROR_NAME_NULL = "shop.user.login.name.null";
    public static final String MSG_ERROR_PASS_NULL = "shop.user.login.pass.null";
    public static final String MSG_ERROR_NULL = "shop.user.login.fail.null";
    public static final String MSG_ERROR_WRONG = "shop.user.login.fail.wrong";

    public static final String VIEW_LOGIN = "login"; //登陆页面
    public static final String VIEW_INDEX = "index"; //首页
    public static final String VIEW_WELCOME = "welcome"; //欢迎页面

    public static final String VIEW_ADMIN = "admin";
    public static final String VIEW_ROLE = "role";
    public static final String VIEW_AUTH = "auth";
    public static final String VIEW_AUTHTYPE = "authtype";
    public static final String VIEW_ADMINPAGEINFO = "adminPageInfo";

    public static final int NUMBER_ONE = 1;
    public static final int NUMBER_TWO = 2;
    public static final int NUMBER_THREE = 3;
    public static final int NUMBER_FORE = 4;
    public static final int NUMBER_FIVE = 5;

}
