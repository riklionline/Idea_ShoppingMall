package com.remcal.util;

/**
 * @ClassName: StringUtil
 * @Descirption:
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 12/13/2019 12:40 PM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
public class StringUtil {

    public  static boolean isEmpty(String str){

        return null == str || "".equals(str);

    }

}
