package com.remcal.util;

import org.springframework.core.convert.converter.Converter;
import org.springframework.expression.ParseException;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @ClassName: CustomDate
 * @Descirption: 全局时间转换工具类
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 12/15/2019 1:57 PM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */

public class CustomerDateConvert implements Converter<String, Date> {

    @Override
    public Date convert(String info) {

        if(info == null || info.isEmpty()){
            return null;
        }
        // 定义数组，保存支持的日期字符串的格式
        SimpleDateFormat[] sdfs = new SimpleDateFormat[]{
                new SimpleDateFormat("yyyy-MM-dd"),
                new SimpleDateFormat("yyyy/MM/dd"),
                new SimpleDateFormat("yyyyMMdd")
        };
        for (SimpleDateFormat sdf : sdfs) {

            // 受检异常、非受检异常
            try {
                try {
                    return sdf.parse(info);
                } catch (java.text.ParseException e) {
                    e.printStackTrace();
                }
            } catch (ParseException e) {
                continue;
            }

        }

        return null;
    }

}
