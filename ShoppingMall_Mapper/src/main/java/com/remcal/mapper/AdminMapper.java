package com.remcal.mapper;

import com.remcal.info.AdminInfo;
import com.remcal.info.RoleInfo;

import java.util.List;

/**
 * @InterfaceName: Admin
 * @Descirption:
 * @Version: V1.01
 * @Author: FAT-Remcal
 * @DateTime: 12/13/2019 1:10 PM
 * @Signature: “闲庭书阁飞玉箫，Coding诗酒醉年华。”
 */
public interface AdminMapper {

    public AdminInfo queryAdminInfoByNameAndPass(AdminInfo adminInfo);
    public List<AdminInfo> queryAdmins(AdminInfo adminInfo);
    public List<AdminInfo> queryAdminsRoles(AdminInfo adminInfo);
    public AdminInfo loginQueryAuth(AdminInfo adminInfo);

}
